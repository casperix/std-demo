package casperix.demo

import casperix.signals.concrete.EmptySignal
import casperix.signals.concrete.Signal

class UIEvents {
    val onRandomize = EmptySignal()
    val onDemo = Signal<DemoEntry>()
}