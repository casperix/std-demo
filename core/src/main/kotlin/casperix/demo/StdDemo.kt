package casperix.demo

import casperix.app.SafeApplicationAdapter
import casperix.demo.component.*
import casperix.gdx.input.registerInput
import casperix.math.matrix.Matrix4f
import casperix.math.vector.Vector2f
import casperix.math.vector.Vector2i
import casperix.math.vector.Vector3f
import casperix.render.Environment
import casperix.render.GdxRenderer2D
import casperix.render.Renderer2D
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputAdapter
import com.badlogic.gdx.utils.ScreenUtils
import ru.casperix.experiment.demo.DemoConfig
import kotlin.random.Random

class StdDemo : SafeApplicationAdapter, InputAdapter() {
    val renderer: Renderer2D = GdxRenderer2D()
    val events = UIEvents()
    val ui = UIController(events)

    private var projection = Matrix4f.IDENTITY
    private var mousePoint = Vector2i.ZERO

    private val random = Random(1)

    private var demoEntry: DemoEntry? = null
    private var demo: Demo? = null

    init {
        registerInput(this)
        resize(Gdx.graphics.width, Gdx.graphics.height)

        events.onDemo.then {
            demoEntry = it
            demo = it.launcher(random)
        }
        events.onRandomize.then {
            refreshDemo()
        }

        events.onDemo.set(CustomDemoList.items.first())
    }

    override fun mouseMoved(screenX: Int, screenY: Int): Boolean {
        mousePoint = Vector2i(screenX, screenY)
        return super.mouseMoved(screenX, screenY)
    }


    private fun refreshDemo() {
        val demoEntry = demoEntry ?: return
        events.onDemo.set(demoEntry)

    }


    override fun resize(width: Int, height: Int) {
        val wRange = width.toFloat() / 2f * DemoConfig.zoom
        val hRange = height.toFloat() / 2f * DemoConfig.zoom
        projection = Matrix4f.orthographic(-wRange, wRange, -hRange, hRange, -100f, 100f)
        ui.uiManager.resize(width, height)
    }

    private fun screenToScene(value: Vector2f): Vector2f {
        val size = Vector2i(Gdx.graphics.width, Gdx.graphics.height).toVector2f()
        return Vector2f(value.x - size.x / 2f, size.y / 2f - value.y) * DemoConfig.zoom

    }

    override fun render() {
        val scenePoint = screenToScene(mousePoint.toVector2f())
        ui.textInfo.text = demo?.update(scenePoint) ?: "<no info>"
        ui.textTitle.text = demoEntry?.name ?: "<no demo>"


        ScreenUtils.clear(0.1f, 0.2f, 0.4f, 1f)

        renderer.begin(Environment( Matrix4f.IDENTITY, projection, Vector3f(500f, 0f, 1000f)))


        demo?.render(renderer)

        renderer.end()

        ui.drawAndUpdate()

    }

    override fun dispose() {

    }
}