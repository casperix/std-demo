package casperix.demo

import casperix.gdx.input.registerInput
import casperix.gdx.input.unregisterInput
import casperix.misc.Disposable
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.utils.viewport.ScreenViewport

class GdxUIManager : Disposable {
    private val viewport = ScreenViewport()
    private val stage = Stage(viewport)
    private var main: Actor? = null

    init {
        registerInput(stage)
    }

    override fun dispose() {
        unregisterInput(stage)
        stage.dispose()
    }

    fun update() {
        stage.act(Gdx.graphics.deltaTime)
        stage.draw()
    }

    fun resize(width: Int, height: Int) {
        viewport.update(width, height, true)
    }

    fun setActor(actor: Actor) {
        main?.let { unsetActor(it) }
        main = actor
        stage.addActor(actor)
    }

    private fun unsetActor(actor: Actor) {
        if (actor == main) {
            main = null
            actor.remove()
        }
    }
}