package casperix.demo.component

import casperix.gdx.graphics.Texture
import casperix.math.axis_aligned.Box2f
import casperix.math.color.ColorCode
import casperix.math.geometry.Line2f
import casperix.math.geometry.fPI
import casperix.math.geometry.rotate
import casperix.math.vector.Vector2f
import casperix.math.vector.toQuad
import casperix.render.Material
import casperix.render.Renderer2D
import kotlin.random.Random


class Render2DDemo(val random: Random) : AbstractDemo() {
    private var angle = 0f
    private var angle2 = fPI
    private val roof_albedo = Texture("roof_albedo.png", true)
    private val roof_normals = Texture("roof_normals.png", true)

    override fun update(customPoint: Vector2f): String {
        angle += 0.011f
        angle2 += 0.017f


        return ""
    }

    override fun render(renderer: Renderer2D) {
        renderer.drawPoint(ColorCode.RED, Vector2f.ZERO, 0.5f)
        renderer.drawLine(ColorCode.GREEN, Line2f(Vector2f.ZERO, Vector2f(5f)), 1f)

        val quad = Box2f(Vector2f.ZERO, Vector2f(3f, 6f)).toQuad().rotate(angle)
        renderer.drawQuad(ColorCode.BLUE, quad)

        val quad2 = Box2f(Vector2f.ZERO, Vector2f(5f, 5f)).toQuad().rotate(angle2)
        renderer.drawQuad(Material(null, roof_albedo, roof_normals), quad2)
        renderer.end()
    }

}


