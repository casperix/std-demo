package casperix.demo.component

import casperix.math.PolarCoordinatef
import casperix.math.color.ColorCode
import casperix.math.curve.SplineCurve
import casperix.math.geometry.fPI2
import casperix.math.nextFloat
import casperix.math.vector.Vector2f
import casperix.math.vector.toDecart
import casperix.render.Renderer2D
import kotlin.random.Random

class SplineCurveDemo(val random: Random) : AbstractDemo() {

    val curve: SplineCurve
    val amount = random.nextInt(2, 6)
    val scale = 10f

    init {
        var last = Vector2f.ZERO
        val points = (0 until amount).map {
            val current = last

            val angle = random.nextFloat() * fPI2
            val range = random.nextFloat(0.2f, 1f) * scale
            last += PolarCoordinatef(range, angle).toDecart()

            current
        }

        curve = SplineCurve(points)
    }

    override fun update(customPoint: Vector2f): String {
        return ""
    }

    override fun render(renderer: Renderer2D) {
        repeat(amount - 1) { t ->
            val color = if (t % 2 == 0) ColorCode.RED else ColorCode.BLUE
            val tStart = t / (amount - 1).toFloat()
            val tFinish = (t + 1) / (amount - 1).toFloat()

            renderer.drawCurve(color, curve, 100, 0.02f, tStart, tFinish)
        }

        repeat(21) {
            val p = curve.getPosition(it / 20f)
            renderer.drawPoint(ColorCode.WHITE, p, 0.05f)
        }
        curve.points.forEach {
            renderer.drawPoint(ColorCode.RED, it, 0.1f)
        }

    }
}