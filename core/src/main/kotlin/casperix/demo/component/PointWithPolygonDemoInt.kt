package casperix.demo.component

import casperix.math.RandomUtil.nextPolygon2i
import casperix.math.color.ColorCode
import casperix.math.intersection.Intersection2i
import casperix.math.vector.Vector2f
import casperix.math.vector.Vector2i
import casperix.render.Renderer2D
import kotlin.random.Random

class PointWithPolygonDemoInt(val random: Random) : AbstractDemo() {

    val polygon = random.nextPolygon2i(-10.. 10, 2..5)
    var point = Vector2i.ZERO
    var hasIntersection = false
    var hasBorder = false


    override fun update(customPoint: Vector2f): String {
        point = customPoint.roundToVector2i()

        hasIntersection = Intersection2i.pointWithPolygon(point, polygon)
        hasBorder = Intersection2i.pointWithPolygonEdge(point, polygon)


        return "Intersection: $hasIntersection\nBorder: $hasBorder"
    }

    override fun render(renderer: Renderer2D) {
        renderer.drawPolygon(ColorCode.GREEN, polygon.convert { it.toVector2f() })

        val pointColor = if (hasBorder) {
            ColorCode.RED
        }else if (hasIntersection) {
            ColorCode.WHITE
        } else {
            ColorCode.BLUE
        }
        renderer.drawPoint(pointColor, point)
    }
}