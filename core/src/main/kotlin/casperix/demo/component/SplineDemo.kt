package casperix.demo.component

import casperix.math.PolarCoordinatef
import casperix.math.color.ColorCode
import casperix.math.curve.Spline
import casperix.math.geometry.fPI2
import casperix.math.nextFloat
import casperix.math.vector.Vector2f
import casperix.math.vector.toDecart
import casperix.render.Renderer2D
import kotlin.random.Random

class SplineDemo(val random: Random) : AbstractDemo() {

    val curve: Spline
    val scale = 10f

    init {
        var last = Vector2f.ZERO
        val points = (0 until 3).map {
            val current = last

            val angle = random.nextFloat() * fPI2
            val range = random.nextFloat(0.2f, 1f) * scale
            last += PolarCoordinatef(range, angle).toDecart()

            current
        }

        curve = Spline(points[0], points[1], points[2])
    }

    override fun update(customPoint: Vector2f): String {
        return ""
    }

    override fun render(renderer: Renderer2D) {
        renderer.drawCurve(ColorCode.RED, curve, 100, 0.02f)

        repeat(21) {
            val p = curve.getPosition(it / 20f)
            renderer.drawPoint(ColorCode.WHITE, p, 0.05f)
        }
        curve.apply {
            listOf(a, b, c).forEach {
                renderer.drawPoint(ColorCode.RED, it, 0.1f)
            }
        }

    }
}