package casperix.demo.component

import casperix.math.RandomUtil.nextTriangle2i
import casperix.math.color.ColorCode
import casperix.math.geometry.Line2i
import casperix.math.intersection.Intersection2i
import casperix.math.vector.Vector2f
import casperix.math.vector.Vector2i
import casperix.render.Renderer2D
import kotlin.random.Random

class PointWithTriangleDemoInt(val random: Random) : AbstractDemo() {

    val triangle = random.nextTriangle2i(-10, 10)
    var point = Vector2i.ZERO
    var hasIntersection = false
    var hasBorder = false


    override fun update(customPoint: Vector2f): String {
        point = customPoint.roundToVector2i()

        hasIntersection = Intersection2i.pointWithTriangle(point, triangle)

        hasBorder = Intersection2i.pointWithLine(point, Line2i(triangle.v0, triangle.v1)) ||
                Intersection2i.pointWithLine(point, Line2i(triangle.v1, triangle.v2)) ||
                Intersection2i.pointWithLine(point, Line2i(triangle.v2, triangle.v0))

        return "Intersection: $hasIntersection\nBorder: $hasBorder"
    }

    override fun render(renderer: Renderer2D) {
        renderer.drawTriangle(ColorCode.GREEN, triangle.convert { it.toVector2f() })

        val pointColor = if (hasBorder) {
            ColorCode.RED
        }else if (hasIntersection) {
            ColorCode.WHITE
        } else {
            ColorCode.BLUE
        }
        renderer.drawPoint(pointColor, point)
    }
}