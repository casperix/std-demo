package casperix.demo.component

import casperix.math.vector.Vector2f
import casperix.render.Renderer2D

interface Demo {
    fun update(customPoint: Vector2f): String

    fun render(renderer: Renderer2D)
}