package casperix.demo.component

import casperix.math.color.Color
import casperix.math.color.ColorCode
import casperix.math.curve.Curve
import casperix.math.geometry.Line2f
import casperix.math.geometry.Polygon2f
import casperix.math.vector.Vector2f
import casperix.math.vector.Vector2i
import casperix.render.Renderer2D
import ru.casperix.experiment.demo.DemoConfig

abstract class AbstractDemo : Demo {
    fun Renderer2D.drawLine(color: Color, line: Line2f) {
        drawLine(color, line, 4f * DemoConfig.zoom)
        drawPoint(color, line.v0, 8f * DemoConfig.zoom)
        drawPoint(color, line.v1, 8f * DemoConfig.zoom)
    }

    fun Renderer2D.drawPoint(color: Color, value: Vector2f) {
        drawPoint(color, value, 8f * DemoConfig.zoom)
    }

    fun Renderer2D.drawCurve(
        color: Color,
        value: Curve,
        steps: Int,
        thick: Float,
        tStart: Float = 0f,
        tFinish: Float = 1f
    ) {
        repeat(steps) {
            val aIndex = it / steps.toFloat()
            val bIndex = (it + 1) / steps.toFloat()


            val a = value.getPosition(aIndex * (tFinish - tStart) + tStart)
            val b = value.getPosition(bIndex * (tFinish - tStart) + tStart)

            drawLine(color, Line2f(a, b), thick)
        }
    }

    fun Renderer2D.drawPoint(color: Color, value: Vector2i) {
        drawPoint(color, value.toVector2f(), 8f * DemoConfig.zoom)
    }

    fun Renderer2D.drawPolygon(color: Color, value: Polygon2f) {

        value.getTriangleList().forEach {
            drawTriangle(color.toColor4f(), it)
        }

        value.getEdgeList().forEach {
            drawLine(ColorCode.WHITE.toColor4f(), it, 4f * DemoConfig.zoom)
        }
    }
}