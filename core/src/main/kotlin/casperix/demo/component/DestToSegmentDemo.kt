package casperix.demo.component

import casperix.math.Math2d.destPointToSegment
import casperix.math.RandomUtil.nextLine2f
import casperix.math.color.ColorCode
import casperix.math.vector.Vector2f
import casperix.misc.toPrecision
import casperix.render.Renderer2D
import kotlin.random.Random

class DestToSegmentDemo(val random: Random) : AbstractDemo() {

    val line = random.nextLine2f(-10f, 10f)

    var customPoint = Vector2f.ZERO

    override fun update(customPoint: Vector2f): String {
        this.customPoint = customPoint
        val dest = destPointToSegment(customPoint.toVector2d(), line.convert { it.toVector2d() })

        return "Dest:" + dest.toPrecision(6)
    }

    override fun render(renderer: Renderer2D) {
        renderer.drawLine(ColorCode.GREEN, line)
        renderer.drawPoint(ColorCode.RED, customPoint)

    }
}