package casperix.demo.component

import casperix.math.RandomUtil.nextLine2f
import casperix.math.RandomUtil.nextVector2f
import casperix.math.color.ColorCode
import casperix.math.geometry.Line2f
import casperix.math.intersection.Intersection2f
import casperix.math.vector.Vector2f
import casperix.render.Renderer2D
import kotlin.random.Random

class LineWithLineDemo(val random: Random) : AbstractDemo() {

    val lineA = random.nextLine2f(-10f, 10f)
    val startB = random.nextVector2f(-10f, 10f)
    var lineB = Line2f(Vector2f.ZERO, Vector2f.ZERO)

    var point: Vector2f? = null

    override fun update(customPoint: Vector2f): String {
        lineB = Line2f(startB, customPoint)

        point = Intersection2f.lineWithLine(lineA, lineB)

        return "Intersection: $point"
    }

    override fun render(renderer: Renderer2D) {
        renderer.drawLine(ColorCode.GREEN, lineA)
        renderer.drawLine(ColorCode.GREEN, lineB)

        point?.let {
            renderer.drawPoint(ColorCode.RED, it)
        }

    }
}