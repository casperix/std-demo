package casperix.demo.component

import casperix.math.RandomUtil.nextTriangle2f
import casperix.math.color.ColorCode
import casperix.math.intersection.Intersection2f
import casperix.math.vector.Vector2f
import casperix.render.Renderer2D
import kotlin.random.Random

class PointWithTriangleDemo(val random: Random) : AbstractDemo() {

    val triangle = random.nextTriangle2f(-10f, 10f)
    var point = Vector2f.ZERO
    var hasIntersection = false


    override fun update(customPoint: Vector2f): String {
        point = customPoint

        hasIntersection = Intersection2f.pointWithTriangle(point, triangle)

        return "Intersection: $hasIntersection"
    }

    override fun render(renderer: Renderer2D) {
        renderer.drawTriangle(ColorCode.GREEN, triangle)

        val pointColor = if (hasIntersection) {
            ColorCode.RED
        } else {
            ColorCode.BLUE
        }
        renderer.drawPoint(pointColor, point)
    }
}