package casperix.demo

import casperix.demo.component.*
import casperix.demo.component.Demo
import kotlin.random.Random

class DemoEntry(val name:String, val launcher:(Random)-> Demo)

object CustomDemoList {
    val items: List<DemoEntry> = listOf(
        DemoEntry("Render2D") { Render2DDemo(it) },
        DemoEntry("Spline") { SplineDemo(it) },
        DemoEntry("Curve") { SplineCurveDemo(it) },
        DemoEntry("PointWithPolygonInt") { PointWithPolygonDemoInt(it) },
        DemoEntry("PointWithTriangleInt") { PointWithTriangleDemoInt(it) },
        DemoEntry("DestToLine") { DestToLineDemo(it) },
        DemoEntry("DestToSegment") { DestToSegmentDemo(it) },
        DemoEntry("LineWithLine") { LineWithLineDemo(it) },
        DemoEntry("PointWithTriangle") { PointWithTriangleDemo(it) },
    )
}