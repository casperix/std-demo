package casperix.demo

import casperix.gdx.ui.addClickListener
import casperix.gdx.ui.size
import casperix.math.vector.Vector2d
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.TextArea
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.utils.Align

class UIController(val events: UIEvents) {
    private val table = Table()
    private val skin = Skin(Gdx.files.internal("assets/uiskin.json"))

    val uiManager = GdxUIManager()
    val textTitle = TextArea("", skin)
    val textInfo = TextArea("", skin)


    init {
        table.setFillParent(true)
        uiManager.setActor(table)

        table.align(Align.topLeft)


        addButton("randomize") { events.onRandomize.set() }

        table.add(textTitle).size(Vector2d(250.0, 40.0))
        table.row()
        table.add(textInfo).size(Vector2d(250.0, 80.0))
        table.row()



        CustomDemoList.items.forEach { demoEntry ->
            addButton(demoEntry.name) { events.onDemo.set(demoEntry) }
        }
    }

    private fun addButton(label: String, click: () -> Unit) {
        table.add(TextButton(label, skin).apply {
            addClickListener {
                click()
            }
        }).size(Vector2d(250.0, 40.0))
        table. row()
    }

    fun drawAndUpdate() {
        uiManager.update()
    }
}